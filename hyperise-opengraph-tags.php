<?php
/*
* Plugin Name: HYPERISE OpenGraph Tags for personalised link previews on LinkedIn
* Description: This plugin leverages Hyperise.com dynamic images to display personalised images in the preview pane, when you share a link with the utm_hyperef parameter added. Facebook OG tags into your blog's single posts which include Blog Title, Post Title, Description and Dynamic Image (if available). 
* Version: 1.7
* Author: HYPERISE
* Author URI: https://hyperise.com
* 
*/

global $EnrichedData,$utm_hyperef; 

// Header Scripts
add_action('wp_head', 'hyperise_opengraphsingle');
add_action('add_meta_boxes', 'hyperise_add_post_meta_box', 1);
add_action('save_post', 'hyperise_save_postdata');
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action('rest_api_init', 'wp_oembed_register_route');
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);


if (isset($_GET['utm_hyperef'])) {
    //JetPack
    add_filter( 'jetpack_enable_open_graph', '__return_false' );
    
    // yeost
    add_filter( 'wpseo_title', 'add_title', 10, 1 );	        
    add_filter( 'wpseo_opengraph_image', 'add_image', 10, 1 );	    
    add_filter( 'wpseo_canonical', 'add_url', 10, 1 );	        
    add_filter( 'wpseo_opengraph_url', 'add_url', 10, 1 );	        
    
    // rankmath https://rankmath.com/kb/filters-hooks-api-developer/
    add_filter( 'rank_math/frontend/title', 'add_title', 10, 1 );
    add_filter( 'rank_math/opengraph/facebook/image', 'add_image', 10, 1);
	add_filter( 'rank_math/opengraph/url', 'add_url', 10, 1);
	add_filter( 'rank_math/frontend/description', 'add_description', 10, 1);
}

function add_description( $str ) {
    global $EnrichedData; 
    if($EnrichedData->m_meta_desc == '') {
        $EnrichedData=hyperise_enrich_description($str);
    }
    $EnrichedData->seoInstalled=true;
    return $EnrichedData->m_meta_desc;
}

function add_url( $str ) {
    global $EnrichedData; 
    if($EnrichedData->utm_hyperef == '') {
        $EnrichedData=hyperise_enrichdata();
    }
    $EnrichedData->seoInstalled=true;
    return $str.'?utm_hyperef='.$EnrichedData->utm_hyperef;
}

function add_title( $str ) {
    global $EnrichedData; 
    if($EnrichedData->m_meta_title == '') {
        $EnrichedData=hyperise_enrich_title($str);
    }
    $EnrichedData->seoInstalled=true;
    return $EnrichedData->m_meta_title;
}
function add_image( $str ) {
    global $EnrichedData; 
    if($EnrichedData->image_url == '') {
        $EnrichedData=hyperise_enrichdata();
    }
    $EnrichedData->seoInstalled=true;
    return $EnrichedData->image_url;
}

function hyperise_save_postdata($post_id){
    if (array_key_exists('hyperise_field', $_POST)) {
        update_post_meta(
            $post_id,
            '_hyperise_meta_key',
            $_POST['hyperise_field']
        );
    }
}

function hyperise_add_post_meta_box() {
    $post_types = get_post_types();
    
    foreach ($post_types as $post_type) {
        if (post_type_supports($post_type, 'editor')) {
            add_meta_box(
                'hyperise_add_post',
                __( 'Hyperise Dynamic Image Settings' ),
                'hyperise_render_editor_box',
                $post_type
            );
        }
    }
}



function hyperise_render_editor_box( $post ) {
	$value = get_post_meta($post->ID, '_hyperise_meta_key', true);
    ?>
    <div id="hyperise-docx-uploader" class="status-empty">
        <table width="100%"><tr><td width="50%" valign="top">
		<p>Step 1: Add Javascript snippet from step 2.1 of the <a href="https://app.hyperise.io/hyper-campaign" target="_blank">Hyper-Campaign</a></p>
		<TEXTAREA name="hyperise_field" id="hyperise_field" placeholder="" style="width:100%; min-height:400px;"><?php echo $value; ?></TEXTAREA>
        </td><td width="50%" valign="top">
		<p>Step 2: Copy and remove the image tag</p>
		<img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/cut_image-1.gif';?>" width="50%" />
		<p>Step 3: Add the image tag to your content</p>
		<img src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/add_image.gif';?>" width="100%" />
	</td></tr></table>    
    </div>
<?php
}


function hyperise_enrichdata(){
    global $post,$EnrichedData,$utm_hyperef; 
    $m_meta_description = get_post_meta($post->ID, '_hyperise_meta_key', true);
    
    if(isset($_GET['utm_hyperef'])){
        $utm_hyperef=sanitize_text_field($_GET['utm_hyperef']);
    }else if(isset($_GET['email'])){
        $utm_hyperef=sanitize_email($_GET['email']);
    }

    //remove everything before <script
    if(strpos($m_meta_description,"<script")>0){
        $m_meta_description=substr($m_meta_description,strpos($m_meta_description,"<script"));
    }

    // find the image ID were using in the JS snippet...		
    $imageIDs = preg_match_all('/imageId: [\'"]([^\'"]+)[\'"].*/i', $m_meta_description, $matches);
    $imageID = $matches[1][0];
          
    // find the image template were using in the JS snippet...		
    $image_templates = preg_match_all('/imageTemplate: [\'"]([^\'"]+)[\'"].*/i', $m_meta_description, $Template_matches);
    $image_template = $Template_matches[1][0];
            
    if(stristr($utm_hyperef,"_") && !stristr($utm_hyperef,"@")){
        $data_source_id=substr($utm_hyperef,0,strpos($utm_hyperef,"_"));
    }else{
        //lets find IF we're using a sheet in the JS snippet...		
        $image_templates = preg_match_all('/sourceId: ([^\,]+)/i', $m_meta_description, $Template_matches);
        $data_source_id = $Template_matches[1][0];
    }

    //Enrich the data based on Hyperise Ref...
    if(strlen($data_source_id)>0 && !stristr($utm_hyperef,"@")){
        //need to preg_match this...
        if(stristr($utm_hyperef,"_")){
            $row_id=substr($utm_hyperef,strpos($utm_hyperef,"_")+1);
        }else{
            $row_id=$utm_hyperef;
        }
        $json_data = wp_remote_get('https://hyperise.com/test/combinedapi.php?image_template='.$image_template.'&row_id='.$row_id.'&data_source_id='.$data_source_id);
        $image_url="https://img.hyperise.io/i/$image_template/sheet-$data_source_id/row-$row_id.png";
    }else{
        //need to preg_match this...
        $json_data = wp_remote_get('https://hyperise.com/test/image_api.php?image_template='.$image_template.'&utm_hyperef='.$utm_hyperef);
        $image_url="https://img.hyperise.io/i/$image_template.png?email=$utm_hyperef";
    } 
    $EnrichedData=json_decode($json_data[body]);
    $EnrichedData->data_source_id=$data_source_id;
    $EnrichedData->image_template=$image_template;
    $EnrichedData->image_url=$image_url;
    $EnrichedData->row_id=$row_id;
    $EnrichedData->imageID=$imageID;
    $EnrichedData->m_meta_description=$m_meta_description;
	
    $EnrichedData->utm_hyperef=$utm_hyperef; 

    return $EnrichedData;
}

function hyperise_enrich_title($title=null)
{
	global $EnrichedData,$post;

	if(!isset($EnrichedData->m_meta_description)) {
		$EnrichedData = hyperise_enrichdata();
	}

	// find the title  were using in the JS snippet...		
	$image_templates = preg_match_all('/title: [\'"]([^\'"]+)[\'"].*/i', $EnrichedData->m_meta_description, $Template_matches);    
	if (strlen($Template_matches[1][0])>0) {
		$EnrichedData->m_meta_title = $Template_matches[1][0];
	}
	else if(!empty($title)) { // use seo title if present
		$EnrichedData->m_meta_title = $title;
	} else {
		$EnrichedData->m_meta_title = $post->post_title;
	}
	
    $EnrichedData->m_meta_title=str_replace("{{first_name}}",$EnrichedData->first_name,$EnrichedData->m_meta_title);       
    $EnrichedData->m_meta_title=str_replace("{{first_name}}",$EnrichedData->first_name,$EnrichedData->m_meta_title);
    $EnrichedData->m_meta_title=str_replace("{{last_name}}",$EnrichedData->last_name,$EnrichedData->m_meta_title);		
    $EnrichedData->m_meta_title=str_replace("{{job_title}}",$EnrichedData->job_title,$EnrichedData->m_meta_title);		
    $EnrichedData->m_meta_title=str_replace("{{business_name}}",$EnrichedData->business_name,$EnrichedData->m_meta_title);
    $EnrichedData->m_meta_title=str_replace("{{website}}",$EnrichedData->website,$EnrichedData->m_meta_title);

	return $EnrichedData;
}

function hyperise_enrich_description($description=null)
{
	global $EnrichedData,$post;

	if(!isset($EnrichedData->m_meta_description)) {
		$EnrichedData = hyperise_enrichdata();
	}

	// find the desc were using in the JS snippet...		
	$image_templates = preg_match_all('/desc: [\'"]([^\'"]+)[\'"].*/i', $EnrichedData->m_meta_description, $Template_matches);
	if(strlen($Template_matches[1][0])>0){
		$EnrichedData->m_meta_desc = $Template_matches[1][0];
	}
	else if(!empty($description)) { // use seo description if present
		$EnrichedData->m_meta_desc = $description;
	} else if(has_excerpt()) {
		$EnrichedData->m_meta_desc = get_the_excerpt();
	} else {
		$EnrichedData->m_meta_desc = substr(strip_tags($post->post_content),0,100);
	}

    $EnrichedData->m_meta_desc=str_replace("{{first_name}}",$EnrichedData->first_name,$EnrichedData->m_meta_desc);
    $EnrichedData->m_meta_desc=str_replace("{{last_name}}",$EnrichedData->last_name,$EnrichedData->m_meta_desc);		
    $EnrichedData->m_meta_desc=str_replace("{{job_title}}",$EnrichedData->job_title,$EnrichedData->m_meta_desc);		
    $EnrichedData->m_meta_desc=str_replace("{{business_name}}",$EnrichedData->business_name,$EnrichedData->m_meta_desc);
	$EnrichedData->m_meta_desc=str_replace("{{website}}",$EnrichedData->website,$EnrichedData->m_meta_desc);
	
	return $EnrichedData;
}


function hyperise_opengraphsingle(){
    if ( is_single()  || is_page() && (isset($_GET['utm_hyperef']) || isset($_GET['email']))) {
	    
    	global $post,$EnrichedData; 
    	
        if($EnrichedData->m_meta_description == '') {
			$EnrichedData=hyperise_enrich_title();			
			$EnrichedData=hyperise_enrichdata();
			$EnrichedData=hyperise_enrich_description();
        }    	
    	
    	echo $EnrichedData->m_meta_description;
    	    	
    	//Replace the page content personalisation tags with enriched data...
    	$content = $post->post_content;		
    	$post->post_content=str_replace("{{utm_hyperef}}",$EnrichedData->utm_hyperef,$post->post_content);
    	$post->post_content=str_replace("{{email}}",$EnrichedData->email,$post->post_content);
    	$post->post_content=str_replace("{{first_name}}",$EnrichedData->first_name,$post->post_content);		
    	$post->post_content=str_replace("{{last_name}}",$EnrichedData->last_name,$post->post_content);		
    	$post->post_content=str_replace("{{profile_image}}",$EnrichedData->profile_url,$post->post_content);
    	$post->post_content=str_replace("{{job_title}}",$EnrichedData->job_title,$post->post_content);		
    	$post->post_content=str_replace("{{business_name}}",$EnrichedData->business_name,$post->post_content);	
    	$post->post_content=str_replace("{{business_industry}}",$EnrichedData->business_industry,$post->post_content);		
    	$post->post_content=str_replace("{{business_address}}",$EnrichedData->business_address,$post->post_content);
    	$post->post_content=str_replace("{{business_lat}}",$EnrichedData->business_lat,$post->post_content);
    	$post->post_content=str_replace("{{business_long}}",$EnrichedData->business_long,$post->post_content);
    	$post->post_content=str_replace("{{business_phone}}",$EnrichedData->business_phone,$post->post_content);										
    	$post->post_content=str_replace("{{logo}}",$EnrichedData->logo_url,$post->post_content);
    	$post->post_content=str_replace("{{website}}",$EnrichedData->website,$post->post_content);
    	$post->post_content=str_replace("{{website_screenshot}}",$EnrichedData->website_screenshot,$post->post_content);		
        
    	$post->post_content=str_replace("{{custom_image_1}}",$EnrichedData->custom_image_1,$post->post_content);		
    	$post->post_content=str_replace("{{custom_image_2}}",$EnrichedData->custom_image_2,$post->post_content);		
    	$post->post_content=str_replace("{{custom_image_3}}",$EnrichedData->custom_image_3,$post->post_content);
    	
    	$post->post_content=str_replace("{{custom_text_1}}",$EnrichedData->custom_text_1,$post->post_content);
    	$post->post_content=str_replace("{{custom_text_2}}",$EnrichedData->custom_text_2,$post->post_content);
    	$post->post_content=str_replace("{{custom_text_3}}",$EnrichedData->custom_text_3,$post->post_content);				
    	$post->post_content=str_replace("{{custom_text_4}}",$EnrichedData->custom_text_4,$post->post_content);		
    	$post->post_content=str_replace("{{custom_text_5}}",$EnrichedData->custom_text_5,$post->post_content);		
    			
    	$post->post_title=$EnrichedData->m_meta_title;
    
    	ob_start();
    	ob_end_clean();
        	
        if($EnrichedData->seoInstalled <> true){
        	
            //Set the OpenGraph Tags, based on the personalised data we have...
            echo '<title>',$EnrichedData->m_meta_title,'</title>';
            echo '<meta property="og:site_name" content="',bloginfo('name'),'"/>';
            echo '<meta property="og:url" content="',the_permalink(),'?utm_hyperef=',$EnrichedData->utm_hyperef,'"/>';
            echo '<meta property="og:title" content="',$EnrichedData->m_meta_title,'"/>';
            if( has_excerpt() ){ 
                echo '<meta property="og:description" content="',$EnrichedData->m_meta_desc,'"/>';
            } else {
                echo '<meta property="og:description" content="',$EnrichedData->m_meta_desc,'"/>';
            }
            if(strlen($EnrichedData->utm_hyperef)>0 && strlen($EnrichedData->image_template)>0){
                echo '<meta property="og:image" content="',$EnrichedData->image_url,'"/>';
            }else{
                echo '<meta property="og:image" content="',esc_attr(get_option( 'default-image' )),'"/>';
            }
        }
    }
}




// Settings Menu
add_action('admin_menu', 'hyperise_opengraphsingle_menu');
function hyperise_opengraphsingle_menu(){
    add_menu_page( 'HYPERISE OpenGraph/OG Tags', 'HYPERISE', 'administrator', 'fb-opengraph-tags', 'hyperise_opengraphsingle_menu_page', 'dashicons-image-filter', '50' );
}

add_action( 'admin_init', 'hyperise_opengraph_options' );

function hyperise_opengraph_options(){
    register_setting( 'meta-data', 'default-image' );
}

function hyperise_opengraphsingle_menu_page(){ ?>
    <div class="wrap">
	<h1 class="ogtitle">HYPERISE OpenGraph/OG Tags Settings</h1>
	<form class="ogdata" action="options.php" method="post">
	<?php settings_fields( 'meta-data' ); ?>
	<?php do_settings_sections( 'hyperise_opengraphsingle_menu' ); ?>
	<h3> Default Image URL </h3>
	<p class="ogpara">If the page doesn't contain any dynamic images then this image will be used instead.</p>
	<input type="text" name="default-image" value="<?php echo esc_attr( get_option( 'default-image' )); ?>">
	<?php submit_button(); ?>
	</form>
	<p class="ogpara">To start making your personalized images, create a free account with <a href="https://hyperise.com" target="_blank">hyperise.com</a></p>
    </div>
    <?php
}

// Load Styles
function hyperise_load_styles(){
    wp_register_style( 'ogstyles', plugin_dir_url( __FILE__ ) . 'assets/css/style.css', false, 'v.1.1' );
    wp_enqueue_style( 'ogstyles' );
}
add_action( 'admin_enqueue_scripts', 'hyperise_load_styles' );

?>